class Mahasiswa(private val umur: Int) {

    // variable global dapat di akses dimanapun selama
    // di dalam lingkup nya
    private val name = "Nama" // variable global
    private val nim = "15.11.8570" // variable global


    fun showInformation() {
        println("Nama : $name")
        println("Nim : $nim")
        println("Umur : $umur")
    }


}

data class Orang(val name: String, val umur: Int)

data class Mahasiswaa(var name: String, val umur: Int, val nim: String = "0")


class Kucing(private val name: String, private val umur: Int, private val gender: String) {

    fun showInformation() {
        println("Nama : $name")
        println("Umur : $umur")
        println("Gender : $gender")
    }

    fun bersuara() {
        println("Meoong ... meongg... ")
    }
}



fun main(args: Array<String>) {

//    val mhs = Mahasiswa(21) // membuat object mhs
//    mhs.showInformation() // mengakses fungsi di dalam object mhs
//    println("Umur : ${mhs.umur}")

//    val orang = Orang("Ulhaq", 21)
//    println("Name : ${orang.name}")
//    println("Umur : ${orang.umur}")

//    val mhs1 = Mahasiswaa("Ulhaq",21)
//    mhs1.name = "ZZZZZZ"

//    val kucing = Kucing("Lili", 2, "Female")
//    kucing.showInformation()
//    kucing.bersuara()

    val calculator = Calculator()
    println("Penjumlahan : ${calculator.penjumlahan(1, 2, 3, 4, 5)}")
    println("Pengurangan : ${calculator.pengurangan(2, 1)}")
    println("Perkalian : ${calculator.perkalian(1, 2)}")
    println("Pembagian : ${calculator.pembagian(4, 2)}")
}

class Calculator {

    fun penjumlahan(vararg x: Int) = x.sum()

    fun pengurangan(a: Int, b: Int) = a - b

    fun perkalian(a: Int, b: Int) = a * b

    fun pembagian(a: Int, b: Int) = a / b

}