Aturan penamaan

class -> Huruf Kapital
	  -> Manusia
	  -> Mahasiswa
	  -> MyClass

variable -> hurufKecil
		 -> isActive
		 -> myNumber
		 -> myName
 konstanta -> MY_NUMBER


function -> hurufKecil
		 -> jumlahBuah(..)
		 -> perkalian(..)
		 -> onOptionClickListener(..)


KOTLIN Visibility Modifiers
 -> public : Bisa diakses di manapun
 -> private : Hanya bisa diakses di class itu sendiri
 -> internal : Hanya bisa diakses di module yang sama
 -> protected : Bisa diakses di class itu sendiri dan turunannya

KOTLIN constructor
- Primary constructor 
- Secondary constructor



